import React from "react";
import { useFormik } from "formik";
import "./Form.css";

const initialValues = {
  name: "",
  lastName: "",
  age: "",
  employed: false,
  color: "",
  sauces: [],
  stooge: "",
  notes: "",
};

const onSubmit = (values) => {
  console.log(values);
};

const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = "Required";
  } else if (!/^[A-Za-z][A-Za-z0-9_]{3,29}$/i.test(values.name)) {
    errors.name = "Please enter a valid name";
  }

  if (!values.lastName) {
    errors.lastName = "Required";
  } else if (!/^[A-Za-z][A-Za-z0-9_]{3,29}$/i.test(values.lastName)) {
    errors.lastName = "Please enter a valid Last Name";
  }

  if (!values.age) {
    errors.age = "Required";
  } else if (!/^(1[6-9]|[2-5]\d|6[0-5])$/i.test(values.age)) {
    errors.age = "Please enter a valid Age";
  }

  if (!/^(\S+\s*){1,100}$/i.test(values.notes)) {
    errors.notes = "Max character for use is 100";
  }

  return errors;
};

function handleSauceChange(sauceName, isChecked, formik) {
  formik.setFieldValue(
    "sauces",
    isChecked
      ? [...formik.values.sauces, sauceName]
      : formik.values.sauces.filter((sauce) => sauce !== sauceName)
  );
}

function UserForm() {
  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  const handleReset = () => {
    formik.resetForm();
  };
  return (
    <div className="form-container">
      <form className="form" onSubmit={formik.handleSubmit}>
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            name="name"
            id="name"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.name}
          />
          {formik.touched.name && formik.errors.name ? (
            <div className="error">{formik.errors.name}</div>
          ) : null}
        </div>

        <div className="form-control">
          <label htmlFor="lastName">Last Name</label>
          <input
            type="text"
            name="lastName"
            id="lastName"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.lastName}
          />
          {formik.touched.lastName && formik.errors.lastName ? (
            <div className="error">{formik.errors.lastName}</div>
          ) : null}
        </div>

        <div className="form-control">
          <label htmlFor="age">Age</label>
          <input
            type="number"
            name="age"
            id="age"
            min={16}
            max={65}
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.age}
          />
          {formik.touched.age && formik.errors.age ? (
            <div className="error">{formik.errors.age}</div>
          ) : null}
        </div>

        <div className="form-control">
          <label htmlFor="employed">Employed</label>
          <input
            type="checkbox"
            name="employed"
            id="employed"
            value={formik.values.employed}
            checked={formik.values.employed}
            onChange={(e) => {
              formik.setFieldValue("employed", e.target.checked);
            }}
          />
        </div>

        <div className="form-control">
          <label htmlFor="color">Favorite Color</label>
          <input
            type="color"
            name="color"
            id="color"
            value={formik.values.color}
            onChange={(e) => {
              formik.setFieldValue("color", e.target.value);
            }}
          />
        </div>

        <h4>Sauces</h4>

        <div className="form-control">
          <input
            type="checkbox"
            name="ketchup"
            id="ketchup"
            checked={formik.values.sauces.includes("ketchup")}
            onChange={(e) => {
              const isChecked = e.target.checked;
              handleSauceChange("ketchup", isChecked, formik);
            }}
          />
          <label htmlFor="sauces">Ketchup</label>
        </div>

        <div className="form-control">
          <input
            type="checkbox"
            name="mayonnaise"
            id="mayonnaise"
            checked={formik.values.sauces.includes("mayonnaise")}
            onChange={(e) => {
              const isChecked = e.target.checked;
              handleSauceChange("mayonnaise", isChecked, formik);
            }}
          />
          <label htmlFor="sauces">Mayonnaise</label>
        </div>

        <div className="form-control">
          <input
            type="checkbox"
            name="guacamole"
            id="guacamole"
            checked={formik.values.sauces.includes("guacamole")}
            onChange={(e) => {
              const isChecked = e.target.checked;
              handleSauceChange("guacamole", isChecked, formik);
            }}
          />
          <label htmlFor="sauces">Guacamole</label>
        </div>

        <div className="form-control">
          <input
            type="checkbox"
            name="mustard"
            id="mustard"
            checked={formik.values.sauces.includes("mustard")}
            onChange={(e) => {
              const isChecked = e.target.checked;
              handleSauceChange("mustard", isChecked, formik);
            }}
          />
          <label htmlFor="sauces">Mustard</label>
        </div>

        <div className="form-control">
          <h4>Best Stooge</h4>
          <label htmlFor="stooge">Larry</label>
          <input
            type="radio"
            name="stooge"
            id="larry"
            value="Larry"
            checked={formik.values.stooge.larry === "Larry"}
            onChange={(e) => {
              formik.setFieldValue("stooge", e.target.value);
            }}
          />
          <label htmlFor="stooge">Moe</label>
          <input
            type="radio"
            name="stooge"
            id="moe"
            value="Moe"
            checked={formik.values.stooge.larry === "Moe"}
            onChange={(e) => {
              formik.setFieldValue("stooge", e.target.value);
            }}
          />
          <label htmlFor="stooge">Curly</label>
          <input
            type="radio"
            name="stooge"
            id="curly"
            value="Curly"
            checked={formik.values.stooge.larry === "Curly"}
            onChange={(e) => {
              formik.setFieldValue("stooge", e.target.value);
            }}
          />
        </div>
        <div className="form-control">
          <label htmlFor="notes">Notes (Max 100 characters)</label>
          <textarea
            id="notes"
            name="notes"
            value={formik.values.notes}
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            maxLength={100}
          />
        </div>
        {formik.touched.notes && formik.errors.notes ? (
          <div className="error">{formik.errors.notes}</div>
        ) : null}
        <button type="submit" disabled={!formik.isValid}>
          Submit
        </button>

        <button type="button" onClick={handleReset}>
          Reset
        </button>
      </form>

      <pre>{JSON.stringify(formik.values, null, 4)}</pre>
    </div>
  );
}

export default UserForm;
